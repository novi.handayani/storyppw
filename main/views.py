from django.shortcuts import render,redirect
from .models import Matkul
from .forms import FormMatkul
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required

response = {}


def home(request):
    return render(request, 'main/home.html')

def rekomendasi(request):
    return render(request, 'main/rekomendasi.html')

@login_required(login_url='/login/')
def tambah_matkul(request):
    form = FormMatkul(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/tambah_matkul')

    context = {'form': form}
    
    return render(request, 'main/formMatkul.html', context)

@login_required(login_url='/login/')
def daftar_matkul(request):
    subjects = Matkul.objects.all() 
    context = {'subjects': subjects}
    return render(request, 'main/daftarMatkul.html', context)

def hapus_matkul(request, pk=None):
    Matkul.objects.get(id=pk).delete()
    return redirect('/daftar_matkul')   

@login_required(login_url='/login/')
def detail_matkul(request, pk=None):
    detail = Matkul.objects.get(id=pk)
    context = {'detail': detail}
    return render(request, 'main/detailMatkul.html', context)
