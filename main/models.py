from django.db import models

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    tahun = models.CharField(max_length=15)
    ruang_kelas = models.CharField(max_length=10)

    def __str__(self):
        return "{}".format(self.nama_matkul)
