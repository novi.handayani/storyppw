from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('rekomendasi/', views.rekomendasi, name='rekomendasi'),
    path('tambah_matkul/', views.tambah_matkul, name='tambah_matkul'),
    path('daftar_matkul/', views.daftar_matkul, name='daftar_matkul'),
    path('hapus_matkul/<int:pk>/', views.hapus_matkul, name='hapus_matkul'),
    path('detail_matkul/<int:pk>/', views.detail_matkul, name='detail_matkul'),
]
