from django import forms
from .models import Matkul

class FormMatkul(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'nama_matkul', 
            'dosen', 
            'jumlah_sks', 
            'deskripsi', 
            'tahun', 
            'ruang_kelas'
        ]

        
    tahun = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Ex: Gasal 2020/2021'})) 
    jumlah_sks = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Masukkan angka'}))  