//Accordion
$(document).ready(function(){
    $('.item-header').click(function(){
        $('.accordion-item').removeClass("active");
        $(this).parent().addClass("active");
        $(".icon").text("+");
        $(this).children(".icon").text("-");
    });
});

//Move Up&Down
$(document).ready(function(){
    var selected;
    var itemlist = $('#accordion');
    var len=$(itemlist).children().length; 
   
     $(".up").click(function(e){
        selected = $(this).parent().index();        
        if(selected>0){  
       		jQuery($(itemlist).children().eq(selected-1)).before(jQuery($(itemlist).children().eq(selected)));
            selected=selected-1;
     	}
    });

     $(".down").click(function(e){
        selected = $(this).parent().index();
        if(selected < len){
        	jQuery($(itemlist).children().eq(selected+1)).after(jQuery($(itemlist).children().eq(selected)));
            selected=selected+1;  
     	}
    });
     
});



//Scroll Up
$(document).ready(function(){    
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    
    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });  
});

// Search Book
$(document).ready(function(){   
    $.ajax({
        url: '/data?q=frozen 2',
        success: function(data){
            var array_items = data.items;
            console.log(array_items);
            $("#daftar_buku").empty(); //kosongkan setiap pemanggilan loop
            $("#daftar_buku").append("<tr><th>Title</th><th>Image</th></tr>");
            for (i = 0; i < array_items.length; i++){
                var title = array_items[i].volumeInfo.title;
                var image = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                var link = array_items[i].volumeInfo.infoLink;
                $("#daftar_buku").append("<tr><td><a href=" + link + " id='book-link' target='_blank'>"+ title + "</a></td><td>" + "<a href=" + link + " target='_blank'><img src=" + image + "></a></td></tr><br>");
            }

        }
    });
    
    $("#search").keyup(function(){
        var inputan = $("#search").val();

        $.ajax({
            url: '/data?q=' + inputan,
            success: function(data){
                var array_items = data.items;
                console.log(array_items);
                $("#daftar_buku").empty(); //kosongkan setiap pemanggilan loop
                $("#daftar_buku").append("<tr><th>Title</th><th>Image</th></tr>");
                for (i = 0; i < array_items.length; i++){
                    var title = array_items[i].volumeInfo.title;
                    var image = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var link = array_items[i].volumeInfo.infoLink;
                    $("#daftar_buku").append("<tr><td><a href=" + link + " id='book-link' target='_blank'>"+ title + "</a></td><td>" + "<a href=" + link + " target='_blank'><img src=" + image + "></a></td></tr><br>");
                }

            }

        });

    });

});