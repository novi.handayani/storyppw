from django.shortcuts import render
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

context = {}
@login_required(login_url='/login/')
def kegiatan(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    context = {'kegiatan': kegiatan, 'orang':orang}

    return render(request, 'story6/kegiatan.html', context)

@login_required(login_url='/login/')
def tambah_kegiatan(request):
    form = FormKegiatan(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/story6/kegiatan')


    context = {'form': form}
    
    return render(request, 'story6/tambahKegiatan.html', context)
  

def register(request, pk):
    daftar = Kegiatan.objects.get(id=pk)
    form = FormOrang(request.POST or None)
    
    if (form.is_valid() and request.method == 'POST'):
        OrangForm = FormOrang(request.POST)
       
        
        daftar.orang.create(nama=form.data['nama'])
     
        return HttpResponseRedirect('/story6/kegiatan')


    context = {'form': form}
    
    return render(request, 'story6/register.html', context)
   
    