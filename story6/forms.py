from django import forms
from .models import Kegiatan, Orang

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['title']

class FormOrang(forms.ModelForm):
    class Meta:
        model = Orang
        fields = ['nama']

