from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import kegiatan
from .models import Kegiatan, Orang

class Story6Test(TestCase):

    def test_apakah_url_kegiatan_ada(self):
        response = Client().get('/story6/kegiatan')
        self.assertEqual(response.status_code,301)

    def test_apakah_url_tambah_kegiatan_ada(self):
        response = Client().get('/story6/tambah_kegiatan')
        self.assertEqual(response.status_code,301)

    def test_apakah_menggunakan_fungsi_dengan_nama_kegiatan(self):
        found = resolve('/story6/kegiatan/')
        self.assertEqual(found.func, kegiatan)
    
    # def test_apakah_menggunakan_template_bernama_kegiatan(self):
    #     response = Client().get('/story6/kegiatan/')
    #     self.assertTemplateUsed(response, 'story6/kegiatan.html')

    # def test_apakah_menggunakan_template_bernama_tambah_Kegiatan(self):
    #     response = Client().get('/story6/tambah_kegiatan/')
    #     self.assertTemplateUsed(response, 'story6/tambahKegiatan.html')

    def test_apakah_model_bisa_membuat_kegiatan_baru(self):
        new_activity = Kegiatan.objects.create(title = 'ngoding')
        counting_all_available_todo = Kegiatan.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    # def test_apakah_di_halaman_kegiatan_ada_teks_Daftar_Kegiatan_dan_tombol_Tambah_Kegiatan(self):
    #     response = Client().get('/story6/kegiatan')
    #     html_kembalian = response.content.decode('utf8')
    #     self.assertIn("Daftar Kegiatan", html_kembalian)
    #     self.assertIn("Tambah Kegiatan", html_kembalian)

    # def test_apakah_model_bisa_membuat_object_Orang(self):
    #          orang_baru = Orang.objects.create(nama = 'Kak Pewe', kegiatan = Kegiatan(title = 'memasak'))
    #          counting_Orang = Orang.objects.all().count()
    #          self.assertEqual(counting_Orang, 1)