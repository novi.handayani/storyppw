from django.db import models

# Create your models here.
class Orang (models.Model):
    nama = models.CharField(max_length=15, null = True)  

    def __str__(self):
        return self.nama

class Kegiatan (models.Model):
    title = models.CharField(max_length=27, null = True)
    orang = models.ManyToManyField(Orang)

    def __str__(self):
        return self.title
