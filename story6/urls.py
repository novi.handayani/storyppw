from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('tambah_kegiatan/', views.tambah_kegiatan, name='tambah_kegiatan'),
    path('register/<int:pk>/', views.register, name='register'),


]