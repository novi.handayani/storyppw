from django.shortcuts import render

def accordion(request):
    return render(request, 'accordion/accordion.html')

