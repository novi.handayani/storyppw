from django.urls import path
from . import views

app_name = 'accordion'

urlpatterns = [
    path('experiences/', views.accordion, name='accordion'),
]