from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion

class AccordionTest(TestCase):
    def test_apakah_url_experiences_ada(self):
        response = Client().get('/experiences/')
        self.assertEqual(response.status_code,200)

    def test_apakah_di_halaman_experiences_ada_templatenya(self):
        response = Client().get('/experiences/')
        self.assertTemplateUsed(response, 'accordion/accordion.html')

    def test_apakah_menggunakan_fungsi_accordion(self):
        found = resolve('/experiences/')
        self.assertEqual(found.func, accordion)

    def test_apakah_di_halaman_experiences_ada_text(self):
        response = Client().get('/experiences/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("My Experiences", html_kembalian)
        self.assertIn("Aktivitas Saat Ini", html_kembalian)
        self.assertIn("Pengalaman Organisasi/Kepanitiaan", html_kembalian)
        self.assertIn("Prestasi", html_kembalian)
        self.assertIn("Hobi", html_kembalian)