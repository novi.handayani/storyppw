from django.urls import path
from . import views

app_name = 'search_book'

urlpatterns = [
    path('search_book/', views.search_book, name='search_book'),
    path('data/', views.data),
]