from django.test import TestCase, Client
from django.urls import resolve
from .views import search_book

class SearchBookTest(TestCase):
    def test_apakah_url_search_book_ada(self):
        response = Client().get('/search_book/')
        self.assertEqual(response.status_code,200)

    def test_apakah_di_halaman_search_book_ada_templatenya(self):
        response = Client().get('/search_book/')
        self.assertTemplateUsed(response, 'search_book/search_book.html')

    def test_apakah_menggunakan_fungsi_search_book(self):
        found = resolve('/search_book/')
        self.assertEqual(found.func, search_book)

    def test_apakah_di_halaman_search_book_ada_text(self):
        response = Client().get('/search_book/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Search Book", html_kembalian)

    def test_apakah_pemanggilan_data_json_sesuai(self):
        response = Client().get('/data/?q=jnsakj/')
        self.assertJSONEqual(response.content.decode("utf-8"), {"kind": "books#volumes", "totalItems": 0})

        