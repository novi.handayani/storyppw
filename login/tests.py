from django.test import TestCase, Client
from django.urls import resolve, reverse
from .forms import CreateUserForm
from django.contrib.auth.models import User

class Story9Test(TestCase):
    # Test url
    def test_apakah_url_register_ada(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code,200)
    
    def test_apakah_url_login_ada(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)
    
    def test_apakah_url_logout_ada(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code,302)

    # Test form
    # def test_forms_login_valid(self):
    #     form_login = CreateUserForm(data={
    #         "username": "novi9",
    #         "password": "vivi9999"
    #     })
    #     self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = CreateUserForm(data={
            "username": "a",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())
        
    def test_forms_register_valid(self):
        form = CreateUserForm(data={
            "username": "",
            "email": "novi11@gmail.com",
            "password1": "19649777",
            "password2": "19649777",
           
        })
        self.assertFalse(form.is_valid())

    # def test_forms_register_valid(self):
    #     form_reg = CreateUserForm(data={
    #         'username': 'novi11',
    #         'email': 'novi11@gmail.com',
    #         'password1': '19649777',
    #         'password2': '19649777',
           
    #     })
    #     self.assertTrue(form_reg.is_valid())     